import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'pages/login.dart';
import 'pages/home.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Color.fromRGBO(0, 0, 0, 0.3), //or set color with: Color(0xFF0000FF)
        systemNavigationBarIconBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.white
    ));
    return new MaterialApp(
      title: 'POP CULTURE',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        fontFamily: 'MPlus',
        platform: TargetPlatform.iOS,
        primaryColor: const Color(0xFF2979FF),
        accentColor: const Color(0xFFff0000),
        scaffoldBackgroundColor: const Color(0xFFFFFFFF),
      ),
      home: new HomePage(),
    );
  }
}
