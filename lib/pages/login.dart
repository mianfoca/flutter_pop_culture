import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'dart:io' show Platform;

import 'package:pop_culture/pages/home.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<FormFieldState<String>> _passwordFieldKey =
      new GlobalKey<FormFieldState<String>>();

  BuildContext _scaffoldContext;
  bool _obscureText = true;
  bool _formWasEdited = false;
  bool _autovalidate = false;

  //validamos el email ingresado
  String _validateEmail(String value) {
    _formWasEdited = true;
    return !value.contains('@') ? 'Ingrese un e-mail válido' : null;
  }

  String _validateString(String value) {
    _formWasEdited = true;
    return value.isEmpty ? 'Ingrese un texto válido' : null;
  }

  //validamos la contraseña
  String _validatePassword(String value) {
    _formWasEdited = true;
    final FormFieldState<String> passwordField = _passwordFieldKey.currentState;
    if (passwordField.value == null || passwordField.value.isEmpty)
      return 'Ingrese una contraseña válida.';
    return null;
  }

  //se llama cuando se da clic en el boton ENVIAR
  void _handleSubmitted() {
    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autovalidate = true; // Start validating on every change.
      showInSnackBar(
          'Por favor corriga los errores antes de enviar el formulario.');
    } else {
      Navigator.of(context).pop({'isLogin': true});
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      backgroundColor: Color(0xFFff1744),
    ));
  }

  @override
  Widget build(BuildContext context) {
    _scaffoldContext = context;

    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return new Scaffold(
      key: _scaffoldKey,
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Builder(builder: (BuildContext context) {
            return new Form(
                key: _formKey,
                autovalidate: _autovalidate,
                child: new SingleChildScrollView(
                  child: new Container(
                    width: width,
                    height: height,
                    padding: EdgeInsets.all(30.0),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Column(
                          children: <Widget>[
                            new SizedBox(height: 30.0),
                            new Text(
                              "Login",
                              style: TextStyle(
                                  fontSize: 27.0, fontWeight: FontWeight.bold),
                            ),
                            new Text(
                              "Signin to your account to continue",
                              style: TextStyle(fontSize: 14.0),
                            )
                          ],
                        ),
                        new Column(
                          children: <Widget>[
                            new TextFormField(
                              validator: _validateEmail,
                              keyboardType: TextInputType.emailAddress,
                              decoration: new InputDecoration(
                                  hintText: '  EMAIL',
                                  border: InputBorder.none,
                                  filled: true,
                                  prefixIcon: Icon(Icons.email)),
                              autofocus: false,
                            ),
                            new SizedBox(height: 20.0),
                            new TextFormField(
                              key: _passwordFieldKey,
                              validator: _validatePassword,
                              decoration: new InputDecoration(
                                hintText: '  PASSWORD',
                                border: InputBorder.none,
                                filled: true,
                                prefixIcon: Icon(Icons.lock),
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                  child: Icon(_obscureText
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                ),
                              ),
                              autofocus: false,
                              obscureText: _obscureText,
                            ),
                            new Row(
                              children: <Widget>[
                                new FlatButton(
                                  onPressed: () {},
                                  child: Text(
                                    "Forgot Password?",
                                    style: TextStyle(
                                        color: Colors.blueAccent,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 0.0, vertical: 12.0),
                                )
                              ],
                              mainAxisAlignment: MainAxisAlignment.end,
                            ),
                            new Center(
                              child: CupertinoButton(
                                child: Container(
                                  width: 200.0,
                                  height: 35.0,
                                  child: new Center(
                                    child: Text(
                                      "Login",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      gradient: LinearGradient(colors: [
                                        // Colors are easy thanks to Flutter's
                                        // Colors class.
                                        Color(0xFF1E88E5),
                                        Color(0xFF1976D2),
                                        Color(0xFF0D47A1),
                                        Color(0xFF0D47A1),
                                      ])),
                                ),
                                onPressed: () {
                                  _handleSubmitted();
                                },
                              ),
                            ),
                            new Row(
                              children: <Widget>[
                                Text(
                                  "Do you have an account?",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(width: 10.0),
                                new FlatButton(
                                  onPressed: () {},
                                  child: Text(
                                    "Register now",
                                    style: TextStyle(
                                        color: Colors.blueAccent,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10.0, vertical: 12.0),
                                )
                              ],
                              mainAxisAlignment: MainAxisAlignment.center,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ));
          })),
    );
  }
}
