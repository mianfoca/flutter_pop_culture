import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:pop_culture/conts.dart';
import 'package:pop_culture/framework/flutter_dina.dart';
import 'package:pop_culture/models/movie.dart';
import 'package:http/http.dart' as http;
import 'package:pop_culture/pages/login.dart';
import 'dart:convert';
import 'dart:math' as math;

import 'package:pop_culture/pages/moviePage.dart';

class Site {
  bool enabled = true;
  bool selected = false;
  final int rowIndex;
  final int columnIndex;

  Site({@required this.rowIndex, @required this.columnIndex, this.enabled});
}

class MovieDetailPage extends StatefulWidget {
  final Movie movie;
  final DinaFramework fr;
  final String tag;

  MovieDetailPage(
      {Key key, @required this.movie, @required this.fr, @required this.tag})
      : super(key: key);

  @override
  _MovieDetailPage createState() => new _MovieDetailPage();
}

class _MovieDetailPage extends State<MovieDetailPage> {
  BuildContext _scaffoldContext;
  var isLogin = true;

  DateTime selectedDate = new DateTime.now(); //dia de la cartelera
  var indexSeletedDay = 0;
  final MAX_TICKETS = 5;
  var scaleSeats = 1.0;
  var xSlider = 0.5;

  List<DateTime> dates = <DateTime>[];

  List<Casting> actores = List<Casting>();
  List<String> imgs = <String>[];
  var tab = 0; //generos de la pelicula
  var numSelected = 0; //numero de asientos seleccionados
  var numTickets = 0; //numero de asientos seleccionados

  var youtube = new FlutterYoutube();
  var trailer = ""; //trailer de la pelicula

  var quality = "IMAX 3D";
  var time = "12H00";

  //matrix to represent sites on the sala
  var sites = [
    [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  ];

  //create matrix of sites with ours props
  List<List<Site>> sitesProps = <List<Site>>[];

  @override
  void initState() {
    super.initState();
    fetchTrailer();

    //creamos un diplicado del array de sitios llenado con objectos de tipo Site
    List<List<Site>> tmpSitesProps = <List<Site>>[];
    var i = 0;
    sites.forEach((row) {
      List<Site> tmpColumn = <Site>[];
      var j = 0;
      row.forEach((column) {
        tmpColumn
            .add(new Site(rowIndex: i, columnIndex: j, enabled: column != 0));
        j++;
      });
      i++;
      tmpSitesProps.add(tmpColumn);
    });

    for (var i = 0; i < 6; i++) {
      dates.add(selectedDate.add(Duration(days: i)));
    }

    setState(() {
      sitesProps = tmpSitesProps;
      dates = dates;
    });
  }

  //metodo para leer las peliculas y convertirlas a un array de movies
  Future<List<Casting>> fetchCast() async {
    if (actores.length > 0) {
      return actores;
    }
    List<Casting> tmp = <Casting>[];
    var url =
        'https://api.themoviedb.org/3/movie/${widget.movie.id}/credits?api_key=$themoviedb';

    final response = await http.get(url);
    if (response.statusCode == 200) {
      var parsed = jsonDecode(response.body);
      for (var item in parsed['cast']) {
        if (Casting.fromJson(item).profile_path != null) {
          tmp.add(Casting.fromJson(item));
        }
      }
      setState(() {
        actores = tmp;
      });
      return tmp;
    } else {
      throw Exception("Failed to load check your internet connection.");
    }
  }

  //get trailer for the movie
  fetchTrailer() async {
    final response = await http.get(
        "https://api.themoviedb.org/3/movie/${widget.movie.id}/videos?api_key=$themoviedb&language=es");
    var parsed = jsonDecode(response.body);
    var tmp = "";
    for (var img in parsed['results']) {
      tmp = img['key'];
      break;
    }
    setState(() {
      trailer = tmp;
    });
  }

  //get images from the movie
  fetchImages() async {
    if (imgs.length > 0) {
      return imgs;
    }
    List<String> tmp = <String>[];
    final response = await http.get(
        "https://api.themoviedb.org/3/movie/${widget.movie.id}/images?api_key=$themoviedb");

    if (response.statusCode == 200) {
      var parsed = jsonDecode(response.body);
      var i = 0;
      for (var img in parsed['backdrops']) {
        if (i < 7) {
          tmp.add(img['file_path']);
          i++;
        } else {
          break;
        }
      }
      setState(() {
        imgs = tmp;
      });
      return tmp;
    } else {
      throw Exception("Failed to load check your internet connection.");
    }
  }

  //activityForResult login page
  goToLoginPage() async {
    //start login page
    Map result = await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => new LoginPage()));

    //check if the user was login
    if (result != null && result.containsKey('isLogin')) {
      setState(() {
        isLogin = result['isLogin'];
      });
    }
  }

  //clean selected sites
  cleanSelected() {
    sitesProps.forEach((row) {
      row.forEach((site) {
        site.selected = false;
      });
    });

    setState(() {
      sitesProps = sitesProps;
      numSelected = 0;
    });
  }

  _buildMovieDetailHeader() {
    return Stack(
      children: <Widget>[
        new Padding(
          padding: EdgeInsets.only(bottom: 30.0),
          child: ClipPath(
            clipper: ArcClipper(),
            child: Image.network(
              "https://image.tmdb.org/t/p/w500/${widget.movie.backdrop_path}",
              width: widget.fr.width,
              height: 290.0,
              fit: BoxFit.cover,
              colorBlendMode: BlendMode.srcOver,
              color: new Color.fromARGB(120, 20, 10, 40),
            ),
          ),
        ),
        new Positioned(
          left: 20.0,
          right: 5.0,
          bottom: 10.0,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Hero(
                  tag: widget.tag,
                  child: new Material(
                    borderRadius: BorderRadius.circular(4.0),
                    elevation: 5.0,
                    child: InkWell(
                      onTap: () {},
                      child: Image.network(
                        themoviedb_img + widget.movie.poster_path,
                        fit: BoxFit.cover,
                        width: widget.fr.wp(34.0),
                        height: widget.fr.hp(26.0),
                      ),
                    ),
                  )),
              Padding(
                padding: EdgeInsets.only(left: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      "IMDb",
                      style: TextStyle(
                          fontSize: 22.0,
                          fontFamily: 'Anton',
                          color: Colors.black54),
                    ),
                    new Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        SizedBox(width: 5.0),
                        Text(
                          widget.movie.vote_average.toString(),
                          style: TextStyle(fontSize: 24.0, color: Colors.black),
                        ),
                        Text(
                          "/10",
                          style: TextStyle(
                              fontSize: 24.0, color: Color(0xFFd5d9d9d9)),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        new Positioned(
            top: 25.0,
            left: 10.0,
            right: 10.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () {
                    Navigator.pop(_scaffoldContext);
                  },
                ),
                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.bookmark_border, color: Colors.white),
                      onPressed: () {
                        Navigator.pop(_scaffoldContext);
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.favorite_border, color: Colors.white),
                      onPressed: () {
                        Navigator.pop(_scaffoldContext);
                      },
                    ),
                  ],
                )
              ],
            )),
        new Positioned(
          bottom: -7.0,
          right: 0.0,
          child: CupertinoButton(
            padding: EdgeInsets.all(0.0),
            pressedOpacity: 0.3,
            onPressed: () {
              youtube.playYoutubeVideoById(
                  apiKey: "AIzaSyBCfP7rIsyrywYhfuRJ4KUIDweeH7mbkSI",
                  videoId: trailer,
                  fullScreen: true //default false
                  );
            },
            child: Stack(
              children: <Widget>[
                ClipPath(
                  clipper: TriClipper(),
                  child: Container(
                    width: widget.fr.width,
                    height: 290 / 2 + 15,
                    color: Colors.amber,
                  ),
                ),
                Positioned(
                  top: 290 / 8,
                  right: widget.fr.width / 12,
                  child: Center(
                      child: Icon(
                    Icons.play_arrow,
                    color: Colors.white,
                    size: 50.0,
                  )),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  //get site color switch enabled or selected
  getSiteColor(Site site) {
    if (site.enabled) {
      if (site.selected) {
        return Colors.amber;
      } else {
        return Colors.black26;
      }
    } else {
      return Colors.transparent;
    }
  }

  _buildSeats() {
    return new Column(
      children: <Widget>[
        //inicio numero de entradas
        new Material(
          color: Color(0xFFF5F5F5),
          child: new Stack(
            children: <Widget>[
              new Positioned(
                  bottom: 30.0,
                  left: -10.0,
                  child: new Transform.rotate(
                      angle: -math.pi / 2,
                      child: new Text(
                        "ENTRADAS",
                        style: TextStyle(fontSize: widget.fr.ip(1.2)),
                      ))),
              new Padding(
                padding: EdgeInsets.only(
                    left: widget.fr.wp(9.0), top: 10.0, bottom: 5.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      "VERMOUTH",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: widget.fr.ip(1.6)),
                    ),
                    new Row(
                      children: <Widget>[
                        new FlatButton(
                            padding: EdgeInsets.symmetric(horizontal: 5.0),
                            color: primary_color,
                            onPressed: () {
                              if (numTickets > 0) {
                                cleanSelected();
                                setState(() {
                                  numTickets = numTickets - 1;
                                });
                              }
                            },
                            child: new Text(
                              "-",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )),
                        new SizedBox(width: 4.0),
                        new Container(
                          height: 37.0,
                          width: 40.0,
                          child: Center(
                            child: Text(numTickets.toString()),
                          ),
                          color: Colors.white,
                        ),
                        new SizedBox(width: 4.0),
                        new FlatButton(
                            padding: EdgeInsets.symmetric(horizontal: 5.0),
                            color: primary_color,
                            onPressed: () {
                              if (numTickets < MAX_TICKETS) {
                                cleanSelected();
                                setState(() {
                                  numTickets = numTickets + 1;
                                });
                              }
                            },
                            child: new Text(
                              "+",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )),
                        new SizedBox(width: 7.0),
                        new Text(
                          "x  \$ 5.00",
                          style: TextStyle(fontSize: widget.fr.ip(1.8)),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        //fin numero de entradas

        new SizedBox(height: 20.0),

        //inicio reservado, disponible, sus asientos
        new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              width: widget.fr.wp(6.0),
              height: widget.fr.wp(6.0),
              decoration: new BoxDecoration(
                  shape: BoxShape.circle, color: primary_color),
            ),
            new SizedBox(width: 4.0),
            new Text("Reservado",
                style: TextStyle(fontSize: widget.fr.ip(1.4))),
            new SizedBox(width: 14.0),
            new Container(
              width: widget.fr.wp(6.0),
              height: widget.fr.wp(6.0),
              decoration: new BoxDecoration(
                  shape: BoxShape.circle, color: Colors.black26),
            ),
            new SizedBox(width: 4.0),
            new Text("Disponible",
                style: TextStyle(fontSize: widget.fr.ip(1.4))),
            new SizedBox(width: 14.0),
            new Container(
              width: widget.fr.wp(6.0),
              height: widget.fr.wp(6.0),
              decoration: new BoxDecoration(
                  shape: BoxShape.circle, color: Colors.amber),
            ),
            new SizedBox(width: 4.0),
            new Text("Sus Asientos",
                style: TextStyle(fontSize: widget.fr.ip(1.4))),
          ],
        ),
        //fin reservado, disponible, sus asientos

        new SizedBox(height: 10.0),

        //inicio pantalla
        new ClipPath(
          clipper: ScreenClipper(),
          child: Container(
            width: 260.0,
            color: primary_color,
            child: Text("PANTALLA",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: widget.fr.ip(1.2),
                    fontWeight: FontWeight.bold)),
          ),
        ),
        //fin pantalla

        new SizedBox(height: 10.0),

        //inicio asientos
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 40.0),
          child: Transform.scale(
              scale: scaleSeats,
              alignment: FractionalOffset(xSlider, 0.5),
              child: new Column(
                children: sitesProps.map((row) {
                  return new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: row.map((site) {
                        return new Container(
                          margin: EdgeInsets.symmetric(vertical: 3.0),
                          width: widget.fr.wp(6.0),
                          height: widget.fr.wp(6.0),
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              color: getSiteColor(site)),
                          child: CupertinoButton(
                              padding: EdgeInsets.all(3.0),
                              child: new Text(''),
                              onPressed: () {
                                if (site.enabled) {
                                  //new selected value for site
                                  final selected = !sitesProps[site.rowIndex]
                                          [site.columnIndex]
                                      .selected;

                                  sitesProps[site.rowIndex][site.columnIndex]
                                      .selected = selected;

                                  if (selected) {
                                    if (numSelected < numTickets) {
                                      numSelected++;
                                    } else {
                                      sitesProps[site.rowIndex]
                                              [site.columnIndex]
                                          .selected = false;
                                    }
                                  } else {
                                    numSelected--;
                                  }

                                  setState(() {
                                    sitesProps = sitesProps;
                                    numSelected = numSelected;
                                  });
                                }
                              }),
                        );
                      }).toList());
                }).toList(),
              )),
        ),
        //fin asientos

        //inicio botones zoom y mover asientos
        new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //inicio btn mover a la izquierda
            new FlatButton(
                color: Colors.amber,
                onPressed: () {
                  if (xSlider >= 0.2) {
                    setState(() {
                      xSlider = xSlider - 0.2;
                    });
                  }
                },
                child: Icon(
                  Icons.arrow_back,
                  size: 25.0,
                  color: Colors.white,
                )),
            //fin btn mover a la izquierda
            new SizedBox(width: 4.0),
            //inicio btn zoom-
            new FlatButton(
                color: primary_color,
                onPressed: () {
                  if (scaleSeats >= 1.1) {
                    setState(() {
                      scaleSeats = scaleSeats - 0.1;
                    });
                  }
                },
                child: Icon(
                  Icons.zoom_out,
                  size: 25.0,
                  color: Colors.white,
                )),
            //fin btn zoom-
            new SizedBox(width: 4.0),
            //inicio btn zoom +
            new FlatButton(
                color: primary_color,
                onPressed: () {
                  if (scaleSeats < 1.3) {
                    setState(() {
                      scaleSeats = scaleSeats + 0.1;
                    });
                  }
                },
                child: Icon(
                  Icons.zoom_in,
                  size: 25.0,
                  color: Colors.white,
                )),
            //fin btn zoom +
            new SizedBox(width: 4.0),
            //inicio btn mover a la derecha
            new FlatButton(
                color: Colors.amber,
                onPressed: () {
                  if (xSlider < 1.0) {
                    setState(() {
                      xSlider = xSlider + 0.2;
                    });
                  }
                },
                child: Icon(
                  Icons.arrow_forward,
                  size: 25.0,
                  color: Colors.white,
                )),
            //fin btn mover a la derecha
          ],
        )
        //fin botones zoom y mover asientos
      ],
    );
  }

  Widget _buildSipnosis() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
      child: Column(
        children: <Widget>[
          Text(
            "Sipnosis:",
            textAlign: TextAlign.start,
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.black),
          ),
          SizedBox(height: 5.0),
          Text(
            widget.movie.overview,
            textAlign: TextAlign.justify,
            style: TextStyle(color: Colors.black54, height: 0.7),
          )
        ],
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }

  Widget _builCast() {
    return new FutureBuilder(
        future: fetchCast(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Casting> actores = snapshot.data;

            return new Column(
              children: <Widget>[
                Padding(
                  child: Text(
                    "Cast",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                  padding: EdgeInsets.only(left: 20.0),
                ),
                SizedBox(height: 5.0),
                Container(
                  child: new ListView(
                    scrollDirection: Axis.horizontal,
                    children: actores.map((cast) {
                      return new Padding(
                        padding: EdgeInsets.only(left: 15.0, bottom: 2.0),
                        child: Column(
                          children: <Widget>[
                            Material(
                              borderRadius: BorderRadius.circular(50.0),
                              child: new CachedNetworkImage(
                                imageUrl:
                                    "https://image.tmdb.org/t/p/w500/${cast.profile_path}",
                                placeholder: Container(
                                  child: new Center(
                                    child: new CircularProgressIndicator(),
                                  ),
                                  width: 100.0,
                                  height: 100.0,
                                ),
                                errorWidget: Container(
                                  child: new Icon(Icons.error),
                                  width: 100.0,
                                  height: 100.0,
                                ),
                                fit: BoxFit.cover,
                                height: 100.0,
                                width: 100.0,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 4.0),
                              child: Text(
                                cast.name,
                                style: TextStyle(
                                    fontSize: 11.0, color: Colors.black54),
                                maxLines: 1,
                              ),
                            )
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                  height: 123.0,
                )
              ],
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
            );
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return new Center(
            child: SpinKitDualRing(
              color: Colors.red,
              size: 80.0,
            ),
          );
        });
  }

  _tab0() {
    return Column(
      children: <Widget>[
        _buildSipnosis(),
        _builCast(),
        SizedBox(
          height: 10.0,
        )
      ],
    );
  }

  _tab1() {
    return new FutureBuilder(
        future: fetchImages(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return new Container(
              padding: EdgeInsets.only(top: 20.0),
              height: widget.fr.height / 2.7,
              child: new Swiper(
                viewportFraction: 0.8,
                scale: 0.9,
                itemWidth: widget.fr.width,
                itemHeight: widget.fr.height / 2.7,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) {
                  return new Image.network(
                    themoviedb_img + imgs[index],
                    fit: BoxFit.cover,
                  );
                },
                itemCount: imgs.length,
                pagination: new SwiperPagination(),
              ),
            );
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return new Padding(
              padding: EdgeInsets.all(40.0),
              child: new Center(
                child: SpinKitDualRing(
                  color: Colors.red,
                  size: 80.0,
                ),
              ));
        });
  }

  _tab2() {
    return Column(
      children: <Widget>[
        new Padding(
          padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
          child: new Column(
            children: <Widget>[
              //inicio dias de la cartelera
              new Container(
                height: 80.0,
                child: new ListView.builder(
                  itemBuilder: (context, index) {
                    var date = dates[index];
                    var btnColor = indexSeletedDay == index
                        ? primary_color
                        : Color(0xFFF5F5F5);

                    var textColor = indexSeletedDay == index
                        ? Colors.white
                        : Colors.black54;

                    return new Container(
                        margin:
                            EdgeInsets.only(top: 5.0, bottom: 5.0, right: 5.0),
                        width: 75.0,
                        height: 75.0,
                        child: new CupertinoButton(
                          padding: EdgeInsets.all(0.0),
                          color: btnColor,
                          onPressed: () {
                            setState(() {
                              indexSeletedDay = index;
                            });
                          },
                          borderRadius: BorderRadius.circular(10.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                getDayName(date.weekday),
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 10.0,
                                    color: textColor,
                                    height: 0.6),
                              ),
                              Text(
                                date.day.toString(),
                                style: TextStyle(
                                    color: textColor,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 30.0),
                              ),
                              Text(
                                getMonth(date.month),
                                style: TextStyle(
                                    color: textColor,
                                    fontWeight: FontWeight.normal,
                                    height: 0.6),
                              )
                            ],
                          ),
                        ));
                  },
                  scrollDirection: Axis.horizontal,
                  itemCount: dates.length,
                ),
              ),
              //fin dias de la cartelera

              new SizedBox(height: 5.0),
              //inicio calidad y hora
              new Row(
                children: <Widget>[
                  new Expanded(
                      child: new MPopupMenu(
                          options: [
                        'IMAX 3D',
                        '2D Doblado',
                        '2D Subtitulado',
                        '3D Doblado'
                      ],
                          value: quality,
                          onSelected: (value) {
                            print(value);
                            setState(() {
                              quality = value;
                            });
                          })),
                  new SizedBox(width: 10.0),
                  new Expanded(
                      child: new MPopupMenu(
                          options: ['11H00', '13H00', '17H00'],
                          value: time,
                          onSelected: (value) {
                            print(value);
                            setState(() {
                              time = value;
                            });
                          }))
                ],
              ),
              //fin calidad y hora
              new SizedBox(height: 10.0),
            ],
          ),
        ),
        new Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.0),
            child: isLogin
                ? _buildSeats()
                : new Column(
                    children: <Widget>[
                      Text(
                        "Debe iniciar sesión para realizar la compra de entradas.",
                        style: TextStyle(color: const Color(0xFFff0000)),
                      ),
                      new Row(
                        children: [
                          Expanded(
                              child: new FlatButton(
                                  onPressed: () {
                                    goToLoginPage();
                                  },
                                  color: primary_color,
                                  child: Text(
                                    "INICIAR SESIÓN",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  )))
                        ],
                      )
                    ],
                  )),
      ],
    );
  }

  _buildTabs() {
    return new Padding(
      padding: EdgeInsets.only(top: 0.0),
      child: Material(
        elevation: 2.0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            new Expanded(
              child: FlatButton(
                child: Text(
                  "INFO",
                  style: TextStyle(
                      color: tab == 0 ? pcolor : Colors.black26,
                      fontFamily: 'Anton',
                      letterSpacing: 2.0),
                ),
                onPressed: () {
                  setState(() {
                    tab = 0;
                  });
                },
                padding: EdgeInsets.all(15.0),
              ),
            ),
            new Expanded(
              child: FlatButton(
                child: Text(
                  "IMAGENES",
                  style: TextStyle(
                      color: tab == 1 ? pcolor : Colors.black26,
                      fontFamily: 'Anton',
                      letterSpacing: 2.0),
                ),
                onPressed: () {
                  setState(() {
                    tab = 1;
                  });
                },
                padding: EdgeInsets.all(15.0),
              ),
            ),
            new Expanded(
              child: FlatButton(
                child: Text(
                  "FUNCIONES",
                  style: TextStyle(
                      color: tab == 2 ? pcolor : Colors.black26,
                      fontFamily: 'Anton',
                      letterSpacing: 2.0),
                ),
                onPressed: () {
                  setState(() {
                    tab = 2;
                  });
                },
                padding: EdgeInsets.all(15.0),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _scaffoldContext = context;
    return new Scaffold(
      body: SingleChildScrollView(
        child: new Column(
          children: [
            _buildMovieDetailHeader(),
            _buildTabs(),
            tab == 0 ? _tab0() : (tab == 1 ? _tab1() : _tab2())
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          elevation: 7.0,
          child: new Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    new Padding(
                        padding: EdgeInsets.only(bottom: widget.fr.hp(0.8)),
                        child: new Text(
                          "Total:",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: widget.fr.ip(1.4)),
                        )),
                    new Text(
                      " \$ ${(numTickets * 5).toStringAsFixed(2)}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: const Color(0xFFF50057),
                          fontSize: widget.fr.ip(3.2)),
                    )
                  ],
                ),
                new CupertinoButton(
                  borderRadius: BorderRadius.circular(25.0),
                  padding:
                      EdgeInsets.symmetric(horizontal: 30.0, vertical: 0.0),
                  onPressed: () {},
                  child: Text(
                    "PAGAR AHORA",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: widget.fr.ip(2.0)),
                  ),
                  color: Colors.green,
                )
              ],
            ),
          )),
    );
  }
}

//dynamic popup menu
class MPopupMenu extends StatelessWidget {
  final String value;
  List<String> options = new List();
  final ValueChanged<String> onSelected;
  double fontSize = 12.0;

  MPopupMenu(
      {Key key,
      @required options,
      @required this.value,
      this.fontSize,
      @required this.onSelected})
      : super(key: key) {
    this.options.addAll(options);
  }

  void onSelectedItem(String value) {
    onSelected(value);
  }

  @override
  Widget build(BuildContext context) {
    return new PopupMenuButton<String>(
        onSelected: onSelectedItem,
        itemBuilder: (BuildContext context) => options.map((option) {
              return PopupMenuItem<String>(value: option, child: Text(option));
            }).toList(),
        child: new Container(
          color: Color(0xFFF5F5F5),
          padding: EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                value,
                style: TextStyle(fontSize: fontSize, color: Colors.black),
              ),
              Icon(
                Icons.arrow_drop_down,
                color: Colors.black,
              )
            ],
          ),
        ));
  }
}

//get month as text
String getMonth(int i) {
  final months = [
    'En',
    'Feb',
    'Mar',
    'Abr',
    'May',
    'Jun',
    'Jul',
    'Ago',
    'Sep',
    'Oct',
    'Nov',
    'Dic'
  ];
  return months[i];
}

//get day as text
String getDayName(int i) {
  final days = ['LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB', 'DOM'];
  return days[i - 1];
}

//clipper to draw "PANTALLA" container
class ScreenClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(0.0, 0.0);
    path.lineTo(20.0, 40.0);
    path.lineTo(240.0, 40.0);
    path.lineTo(260.0, 0.0);
    path.close();
    return path;

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
